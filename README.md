# Dashboard Service
The dashboard service exists to register administered Covid-19 dashboards.

## Installing Helm Chart locally
For development purposes, you may need to modify and install the modified Helm Chart locally. This can be done with the
following command:
```
helm upgrade --install release charts/dashboard-service
```