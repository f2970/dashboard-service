package nl.fruitsinc.dashboard.query

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QueryApplication

fun main(args: Array<String>) {
    runApplication<QueryApplication>(*args)
}
